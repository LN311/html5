HTML5、CSS3基础
==============================
# 目标
* 了解并使用重点的HTML5新元素和新特性
* 了解并使用重点的CSS新属性，了解CSS3 2D，3D动画原理
* 了解响应式设计基础知识
* 通过所学知识，动手实现一个简单的HTML5网站

# HTML5基础
## 从HTML4到HTML5
* 语义化
* 新特性

## HTML5新增元素和特性
### 重点元素
[语义元素](http://www.runoob.com/html/html5-semantic-elements.html)
### 交互元素
[内容交互](http://www.w3school.com.cn/tags/tag_details.asp)
[状态交互](http://www.w3school.com.cn/tiy/t.asp?f=html5_progress)
### HTML5中的表单
[Input类型](http://www.runoob.com/html/html5-form-input-types.html)
### HTML5多媒体
[视频](http://www.runoob.com/html/html5-video.html)
[音频](http://www.runoob.com/html/html5-audio.html)
### HTML5画布
本部分不做重点
[白鹭引擎](https://egret.com/case)
[莽荒纪](http://gamecenter.egret-labs.org/Mine.play?gameId=90488&amp;chanId=20409)

## HTML5数据存储
* 不做重点

# CSS3基础
## CSS3选择器

## CSS3新属性
[边框](http://www.w3school.com.cn/css3/css3_border.asp)
[背景](http://www.w3school.com.cn/css3/css3_background.asp)
[文本效果](http://www.w3school.com.cn/css3/css3_text_effect.asp)
[字体](http://www.w3school.com.cn/css3/css3_font.asp)

## CSS3特效
### CSS3位移与变形处理
[变形：transform](https://www.w3cplus.com/content/css3-transform)
[过渡: transition](https://www.w3cplus.com/content/css3-transition)
[动画: animation](https://www.w3cplus.com/content/css3-animation)

### CSS3 2D转换与过渡动画
[2D转换](http://www.w3school.com.cn/css3/css3_2dtransform.asp)
[2D过渡](http://www.w3school.com.cn/tiy/t.asp?f=css3_transition1)

### CSS3 3D转换与关键帧动画
[3D转换](http://www.w3school.com.cn/css3/css3_3dtransform.asp)
[关键帧动画](http://www.w3school.com.cn/css3/css3_animation.asp)

# Html5+CSS3布局
## Flex布局
[Flex布局](http://www.ruanyifeng.com/blog/2015/07/flex-grammar.html)
[Flex实例](http://www.ruanyifeng.com/blog/2015/07/flex-examples.html)
## 媒体查询
[媒体查询](https://www.w3.org/html/ig/zh/wiki/CSS3%E5%AA%92%E4%BD%93%E6%9F%A5%E8%AF%A2)
## 响应式设计


# 实体网站练习