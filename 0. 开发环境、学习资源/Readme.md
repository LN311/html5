开发环境、学习资源
======================

# 1. Google Chrome
## 安装
[Google Chrome](https://www.google.com/chrome/)

## 介绍
* Javascript执行效率
* 对html5+css3的兼容
* 设计简洁
* 对网页的设计和调试

# 2. Nginx(Http Web容器)
## 安装
[Nginx 官网](https://nginx.org/en/download.html)

## Http (HyperText Transfer Protocol)
* 目的是保证客户机与服务器之间的通信
* 客户端向服务端指定端口请求数据，服务端返回资源
* 统一资源标识符（URL）
* [GET/POST](http://www.w3school.com.cn/tags/html_ref_httpmethods.asp)

## 介绍
* Web Server (apache, tomcat, IIS等)
* Proxy Server
* 现场搭建一个网站 

## 使用
* 获取帮助信息
`nginx -h`
* 启动nginx
在windows里：`start nginx`
Mac OS/Linux：`nginx` 
* 修改配置后重新启动nginx
`nginx -s reload `
* 停止nginx
`nginx -s stop`

## 配置
* 通过`nginx -h`找到配置路径:/usr/local/etc/nginx/nginx.conf
* 基本配置
```
server{
    listen 8080;
    server_name localhost;

    location / {
        root /Users/zzm/workspace/zkyh/www;
        index index.html index.htm;
    }
}
```
>root : http://localhost:8080访问的根目录
>index: 在url中没有path的情况下，默认打开的文件。

# 3. Visual Studio Code
## 安装与配置
[Visual Studio Code 下载地址](https://code.visualstudio.com/download)

## 简介和使用
* Explorer 
* Search
* Source Control (版本控制，git)
* Debug （调试，实习生产品，chrome）
* Extensions (程序扩展)

# 4. Node.js
## 安装
https://nodejs.org/en/
## 介绍
### Javascript运行环境
* 基于Chrome V8引擎
* 事件驱动、非阻塞式I/O
* 轻量高效
### npm包管理器 (Node Package Manager)
* 全球最大的开源生态库
* 安装第三方一赖

# 5. Git/SVN(版本控制工具)
## 安装GIT
[桌面版本](https://desktop.github.com/)
[Git命令行](https://git-scm.com/downloads)
还有前面的Visual Studio Code。
## 安装SVN
[乌龟](https://tortoisesvn.net/downloads.html)

## 介绍
* 注册github账号,下载Demo
* 公司提供svn账号，提交作业

# 6. 技术文档
[MDN](https://developer.mozilla.org/zh-CN/)
* 字典