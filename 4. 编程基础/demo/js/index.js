function get(options){
    var data = options.data;
    var callback = options.onSucceed;
    var http = new XMLHttpRequest();
    http.onload = function(){
        var json = JSON.parse(this.responseText);
        callback && callback(json);
    }
    http.open("GET", options.url, true)
    http.send();
}

function loadBrandWalls(){
    get({
        url:"./api/brand/walls.json",
        onSucceed:function(d){
            var wall = document.getElementById('brand-wall');
            var item = wall.children[0];
            wall.removeChild(item);

            for(var i = 0; i < d.length; i++){
                var it = d[i];

                var c = item.cloneNode(true);
                var ss = c.getElementsByTagName("span");
                ss[0].innerHTML = it.name;
                ss[1].innerHTML = it.en;

                var aa = c.getElementsByTagName("a");
                for(var a of aa)
                    a.href = it.url;
                
                var img = aa[1].getElementsByTagName('img');
                img[0].src = it.image;

                if(i == 0)
                    c.className += " first";
                else if(i == d.length - 1)
                    c.className += " last";

                wall.appendChild(c);
            }
        }
      });
}

function loadBrandList(){
    var ul = document.getElementById("brand-list");
    var li = ul.children[0];

    ul.removeChild(li);

    for(var i = 0; i < 20; i++){
        
        var cli = li.cloneNode(true);
        var img = cli.getElementsByTagName('img')
        if(i % 2 == 0){
            img[0].src = "./images/brands/uyk.png";
        }
        else{
            img[0].src = "./images/brands/zqjd.png";
        }

        ul.appendChild(cli);
    }
}