module.exports = {
    serverException:'serverException',
    accountNotFound:'accountNotFound',
    passwordNotFound:'passwordNotFound',
    userNotFound:'userNotFound',
    tokenExpired:'tokenExpired',
    sessionNotFound:'sessionNotFound',
    accountNotFound:'accountNotFound',
    passwordNotFound:'passwordNotFound',
    nickNotFound:'nickNotFound',
    accountExists:'accountExists',
    invalidParams:'invalidParams'
}