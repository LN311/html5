const exp = require('express');
const router = exp.Router();
const db = require('../db/db');
const _ = require('../libs/global');
const err = require('../libs/errors');

router.post('/list', (req, res)=>{
    var data = res.body;
    //在brands中找到所有的品牌信息
    var brands = db.list('brands');
    //按照visist排序(visit表示热度)
    brands.sort((l,r)=> r.visist - l.visist);
    //最多返回30条
    var walls = [];
    var i = 0;
    while(i < 20 && i < brands.length){
        walls.push(brands[i++]);
    }
    res.json({stat:"ok", brands:brands});
})

router.post('/walls', (req, res)=>{
    //在brands中找到所有的品牌信息
    var activities = db.list('activities');
    //最多返回30条
    var ret = [];
    var i = 0;
    while(i < 30 && i < activities.length){
        ret.push(activities[i++]);
    }
    res.json({stat:"ok", walls:ret});
})


module.exports = router;