const exp = require('express');
const router = exp.Router();
const db = require('../db/db');
const _ = require('../libs/global');
const err = require('../libs/errors');
const kit = require('../kit/session')

router.post('/all', (req, res)=>{
    var data = res.body;
    var mid = data.mid;
    //返回modules中所有的modules信息
    var modules = db.list('modules');
    res.json({stat:"ok", modules:modules});
})

router.post('/detail', (req, res)=>{
    var data = req.body;
    var mid = data.mid;
    //1. TODO: 校验数据合法性
    _.verify(mid > 0, err.invalidParams);
    //2. TODO: 在modules中找到module的信息
    var _module = {};
    var products = [];
    //3. TODO: 根据products中的id得到对应的product信息
    
    res.json({stat:"ok", module:{
        id:_module.id,
        name:_module.name,
        pic:_module.pic,
        keys:_module.keys,
        products:products
    }});
})

router.post('/wonderful', (req, res)=>{
    var data = req.body;
    var token = data.token;
    //1. 通过session找到user
    var s = token ? kit.getSession(token) : null;
    var user = s ? db.get('users', x=> x.id == s.uid):null;

    let products = [];
    if(user){
        //2.1 通过用户id找到marks中对应用户被打的标签
        var marks = db.list('marks', x => x.uid == user.id);
        //2.2 TODO: 在products中找到名字包含标签的产品
        //注：达到30个就返回产品信息, 如果所有产品都没有30个，就返回查询到的结果   
        
    }
    else{

        //3.这里是没有用户信息的情况，
        //3.1 TODO: 按照marks中visist的数量进行排序，
        var marks = db.list('marks');
        //3.2 TODO: 从visist最高的mark来找产品，直到达到30个产品就返回产品信息
        //注：如果所有产品信息都没有30个，则返回查询到的结果
        
    }
    res.json({stat:"ok", products:products});
})


module.exports = router;