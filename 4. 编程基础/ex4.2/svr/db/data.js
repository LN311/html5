const users = require('./users');
const marks = require('./marks');
const products = require('./products');
const activities = require('./activities');
const modules = require('./modules');
const brands = require('./brands')

module.exports = {
    users:users,
    marks:marks,
    products:products,
    activities:activities,
    modules:modules,
    brands:brands,
    primary:{
        users:2,
        marks:100,
        products:100,
        activities:100,
        modules:100,
        brands:100
    }
}