

module.exports = (app, options)=>{
    app.use('/api/auth', require('./api/auth'));
    app.use('/api/brand', require('./api/brand'));
    app.use('/api/module', require('./api/module'));
}