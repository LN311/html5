import $ from 'jquery';
import cookie from 'jquery.cookie'
import './assets/css/style.css';
import './assets/css/index.css';


class IndexControl{
    checkUserInfo(){
        //TODO: 实现免登录
    }

    bindEvents(){
        //TODO: 实现登录相关事件的绑定
    }

    loadBrandWalls(){
        var con = document.getElementById('brand-wall');
        $(con).hide();
        $.ajax({
            url:'/api/brand/walls',
            method:'post'
        }).done(d=>{
            if(d.stat == 'ok'){
                this._renderBrandWalls(con, d.walls);
                $(con).show();
            }
        });
    }

    _renderBrandWalls(wall, walls){
        var item = wall.children[0];
        wall.removeChild(item);

        for(var i = 0; i < walls.length; i++){
            var it = walls[i];

            var c = item.cloneNode(true);
            var ss = c.getElementsByTagName("span");
            ss[0].innerText = it.name;
            ss[1].innerText = it.en.toUpperCase();

            var aa = c.getElementsByTagName("a");
            for(var a of aa)
                a.href = "/brand/" + it.id;
            
            var img = aa[1].getElementsByTagName('img');
            img[0].src = it.image;

            if(i == 0)
                c.className += " first";
            else if(i == walls.length - 1)
                c.className += " last";

            wall.appendChild(c);
        }
    }

    loadBrandList(){
        var ul = document.getElementById("brand-list");
        $(ul).hide();
        $.ajax({
            url:'/api/brand/list',
            method:'post'
        }).done(d=>{
            if(d.stat == 'ok'){
                this._renderBrandList(ul, d.brands);
                $(ul).show();
            }
        });
    }

    _renderBrandList(ul, brands){
        var li = ul.children[0];

        ul.removeChild(li);

        for(var k in brands){
            var b  = brands[k];
            var cli = li.cloneNode(true);
            var img = cli.getElementsByTagName('img')
            img[0].src = b.pic;
            ul.appendChild(cli);
        }
    }

    loadTmallMarket(){
        var con = document.getElementById('tmall-market');
        $(con).hide();
        $.ajax({
            url:'/api/module/detail',
            method:'post',
            data:{mid:1}
        }).done(d=>{
            if(d.stat == 'ok'){
                this._renderTmallMarket(con, d.module);
                $(con).show();
            }
        });
    }

    _renderTmallMarket(con, detail){
        var hwCon = con.getElementsByClassName('floor-hot-word-con')[0];
        var hwLink = hwCon.children[0];
        hwCon.removeChild(hwLink);
        for(let i in detail.keys){
            var a = hwLink.cloneNode(true);
            a.href = '#';
            a.innerText = detail.keys[i];
            hwCon.appendChild(a);
        }
        
        $('.floor-banner-left a img').attr('src', detail.pic);


        var grid = con.getElementsByClassName('floor-banner-right')[0];
        var pLink = grid.children[0];
        grid.removeChild(pLink);

        for(let i = 0; i < detail.products.length; i++){
            var p = detail.products[i];
            var a = pLink.cloneNode(true);

            //TODO:根据product信息，把dom中对应的项目完成。

            if(i % 4 == 3){
                p.className += ' last';
            }

            grid.appendChild(a);
        }
    }

    loadWonderful(){
        var token = $.cookie('token');
        var con = document.getElementById('wonderful');
        $(con).hide();
        $.ajax({
            url:'/api/module/wonderful',
            method:'post',
            data:{token:token}
        }).done(d=>{
            if(d.stat == 'ok'){
                this._renderWonderful(con, d.products);
                $(con).show();
            }
        });
    }

    _renderWonderful(ul, products){
        var li = ul.children[0];

        ul.removeChild(li);

        
        for(let k = 0; k < products.length; k++){
            var p  = products[k];
            var cli = li.cloneNode(true);
            
            //TODO:根据product信息，把dom中对应的项目完成。

            if(k % 5 == 4)
                cli.className += ' last';

            ul.appendChild(cli);
        }
    }
}

$(document).ready(e=>{
    const ctrl = new IndexControl();
    ctrl.checkUserInfo();
    ctrl.bindEvents();
    ctrl.loadBrandWalls();
    ctrl.loadBrandList();
    ctrl.loadTmallMarket();
    ctrl.loadWonderful();
})