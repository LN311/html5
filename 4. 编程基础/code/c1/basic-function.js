//apply

function add(x, y){
    return x + y;
}

var min = function(x, y){
    return x > y ? y : x;
}

function Point(x, y){
    this.x = x;
    this.y = y;

    this.show = function(){
        console.log(this.toString());
    }
}

Point.prototype.toString = function(){
    return "(" + this.x + "," + this.y + ")";
}

var p = new Point(1, 2);
p.show();
