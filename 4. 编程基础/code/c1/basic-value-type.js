


//var a = 0;
// var f = 1.0;//float
// var b = 'text';
// var c = { "a": a};
// var d = [a, b, c, 1, null, undefined, NaN];

//console.log(typeof(a) +  ":" + b + ":" + c + ":" + d + ":" + f);

//console.log(`${a} : ${b} : ${c} : ${d} : ${f}`);
//console.log(`${typeof(a)} : ${typeof(b)} : ${typeof(c)} : ${typeof(d)} : ${typeof(f)} : ${typeof(null)} : ${typeof(undefined)} : ${typeof(NaN)}`);


//var m;
//console.log(typeof(m));

//var a = NaN;
//var b = NaN;

//console.log(a == b);


var a = "1";
var b = 1;


var c = a + b;

// ==/===
console.log(`1 == "1" is ${1 == "1"}`); //true
console.log(`1 == 1.0 is ${1 == 1.0}`); //true
console.log(`0.0025 === 1/400 is ${0.0025 === 1/400}`); //true (c++)

console.log(`1 === "1" is ${1 === "1"}`); //?
console.log(`1 === 1.0 is ${1 === 1.0}`); //true


console.log(`true == "true" is ${true == "true"}`);
console.log(`true === "true" is ${true === "true"}`);

//NaN
console.log(`null == null is ${null === null}`);
console.log(`undefined == undefined is ${undefined === undefined}`);
console.log(`NaN == NaN is ${NaN == NaN}`); //?

var b = parseInt("a1111");  //b=NaN
var c = parseInt("a1111");   //c=NaN
console.log("b==c" + (b == c))

var text1 = "Hello World!";       // String
var text2 = "HeLLo WORLD!"

var a = {"b":1};
var p = "B";
console.log(a[p.toLocaleLowerCase()])

if(text2.toLocaleLowerCase() == text1.toLocaleLowerCase()){

}

//number
var a = 1;
Number(true);          // returns 1
Number(false);         // returns 0
Number("10");          // returns 10 parseInt("10")
Number("  10");        // returns 10
Number("10  ");        // returns 10
Number("10 20");       // returns NaN 
Number("John");        // returns NaN


//string
//https://www.w3schools.com/js/js_string_methods.asp
console.log("123".length);
console.log("中国".length);
console.log("中国"[0]);
console.log("中国".charAt(0));
console.log("中国".charCodeAt(0));
//slice/sub/substring/ indexOf/lastIndexOf/search /split/replace/contact
console.log("中国".slice(1));


//正则表达式

var str = "Please locate where 'locate' occurs!";
var pos = str.search(/where/g);
console.log("==========> " + pos);


//object
var car = {type:"Fiat", model:"500", color:"white"};


//array
//https://www.w3schools.com/js/js_array_methods.asp
var c = ["Banana", "Orange", "Apple", "Mango"].length;console.log(c);
c = ["Banana", "Orange", "Apple", "Mango"][0];console.log(c);
c = ["Banana", "Orange", "Apple", "Mango"].toString();console.log(c);

c = ["Banana", "Orange", "Apple", "Mango"].join("|")
console.log(c);

var xx = c.split('|');


//pop/push/shift/unshift/splice/slice/contact

//sort
var points = [{x:1, y:1}, {x:2, y:2}, {x:1, y:3}, {x:6, y:4}, {x:2, y:5}, {x:1, y:6}];

//var first = points.pop();
//var _first = points.shift();

points.sort(function(l, r){
    if(r.x > l.x){
        return 1;
    }
    else if(r.x < l.x){
        return -1;
    }

    return r.y - l.y;
})

console.log(points.map(x=>{return `(${x.x}, ${x.y})`}).join(","));

//points.sort(function(a, b){return 0.5 - Math.random()});

//Date

//json




