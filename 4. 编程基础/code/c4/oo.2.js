//1. 继承

class Pet{
    constructor(name, type){
        this.name = name;
        this._type = type;
    }

    getType(){
        return this._type;
    }

    sayHello(){
        console.log(`Hello, my name is ${this.name}, i'm a ${this._type}`);
    }

    play(){
        this.sayHello();
        switch(this.type){
            case 'cat':console.log("i'm catching mouse");break;
            case 'dog':console.log("i'm barking");break;
            default:
                console.log("i'm playing");break;
        }
    }
}


class Cat extends Pet{
    constructor(name){
        super(name, 'cat');
    }

    _doing(){
        console.log("i'm catching mouse");
    }

    play(){
        this.sayHello();
        this._doing();
    }
}

class Dog extends Pet{
    constructor(name){
        super(name, 'dog');
    }

    _doing(){
        console.log("i'm barking");
    }

    play(){
        this.sayHello();
        this._doing();
    }
}

var kitty = new Cat('kitty');
kitty.play();

var tom = new Dog('tom');
tom.play();

for(var i = 0; i < 10; i++){
    if(i % 2 == 0){
        new Cat('kitty' + i).play();
    }
    else{
        new Dog('tom' + i).play();
    }
}

//
