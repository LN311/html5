//1. 封装
var kitty = {
    type:'cat',
    name:'kitty'
};

var tom = {
    type:'dog',
    name:'tom'
}

function sayHello(pet){
    console.log(`Hello, my name is ${pet.name}, i'm a ${pet.type}`);
}

sayHello(kitty);
sayHello(tom);



class Pet{
    constructor(name, type){
        this.name = name;
        this._type = type;
    }

    getType(){
        return this._type;
    }

    sayHello(){
        console.log(`Hello, my name is ${this.name}, i'm a ${this._type}`);
    }
}

var _tom = new Pet('tom', 'dog');
var _kitty = new Pet('kitty', 'cat');
_tom.sayHello();
_kitty.sayHello();

//1. 抽象
//2. 数据给可信对象调用
