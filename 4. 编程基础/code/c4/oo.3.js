//1. 多态 (覆盖)
//http://www.cnblogs.com/clongge/archive/2008/07/09/1239076.html

class Pet{
    constructor(name, type){
        this.name = name;
        this._type = type;
    }

    getType(){
        return this._type;
    }

    sayHello(){
        console.log(`Hello, my name is ${this.name}, i'm a ${this._type}`);
    }

    play(){
        this.sayHello();
        this._doing();
    }

    _doing(){
        
    }
}


class Cat extends Pet{
    constructor(name){
        super(name, 'cat');
    }

    _doing(){
        console.log("i'm catching mouse");
    }
}

class Dog extends Pet{
    constructor(name){
        super(name, 'dog');
    }

    _doing(){
        console.log("i'm barking");
    }
}

class Pig extends Pet{
    constructor(name){
        super(name, 'pig');
    }

    sayHello(){
        console.log(`HONG~HONG~, i'm ${this.name}, a little ${this._type}`);
    }

    _doing(){
        console.log("i'm sleeping, HONG~HONG~");
    }
}

var kitty = new Cat('kitty');
kitty.play();

var tom = new Dog('tom');
tom.play();

var xx = new Pig('xiaoxin');
xx.play();




class Person{
    constructor(pet){
        if(pet instanceof Pet)
            this.pet = pet; //Pet
        else
            throw 'not a pet';
    }

    walk(){
        console.log("i'm the master of " + this.pet.name);
        this.pet.play();
        console.log("bye,bye!!");
    }
}

new Person(tom).walk();
