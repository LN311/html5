import $ from 'jquery';
import cookie from 'jquery.cookie'
import './assets/css/style.css';


$("#login").click(e=>{
    $.ajax({
        url:'/api/auth/login',
        method:'post',
        data:{account:"marry@gmail.com", password:"123qwe"}
    }).done(d=>{
        $('#login-layout').hide();
        $('#user-info').show();
        $('#account').text(d.user.account);
        $.cookie('token', d.session.token,{ expires:7});
    })
})

$("#regist").click(e=>{
    $.ajax({
        url:'/api/auth/regist',
        method:'post',
        data:{account:"kitty@gmail.com", password:"123qwe", nick:'kitty'}
    }).done(d=>{
        $('#login-layout').hide();
        $('#user-info').show();
        $('#account').text(d.user.account);
        $.cookie('token', d.session.token, {expires:7});
        $('#account').text(d.user.account);
    })
})

$("#logout").click(e=>{
    let token = $.cookie('token');
    $.ajax({
        url:'/api/auth/logout',
        method:'post',
        data:{token:token}
    }).done(d=>{
        $('#login-layout').show();
        $('#user-info').hide();
        $.removeCookie('token');
    })
})

$(document).ready(e=>{
    let token = $.cookie('token');
    if(!token){
        $('#user-info').hide();
        $('#login-layout').show();
        return;
    }

    $('#user-info').hide();
    $.ajax({
        url:'/api/auth/check',
        method:'post',
        data:{token:token}
    }).done(d=>{
        if(d.stat == 'ok'){
            $('#login-layout').hide();
            $('#user-info').show();
            $('#account').text(d.user.account);
        }
    });
})