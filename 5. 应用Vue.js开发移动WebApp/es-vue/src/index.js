import Vue from 'vue'
import VueRouter from 'vue-router'
import iView from 'iview';

import store from './stores/store'
import routes from './routes'
import './assets/css/style.css'
import 'iview/dist/styles/iview.css'

import vueQuillEditor from 'vue-quill-editor'
import 'quill/dist/quill.core.css'
import 'quill/dist/quill.snow.css'
import 'quill/dist/quill.bubble.css'


Vue.use(iView);
Vue.use(VueRouter);
Vue.use(vueQuillEditor);

const router = new VueRouter({
  routes,
  mode: 'hash'
})

new Vue({
  el: '#app',
  data: store,
  router,
  template: `<router-view></router-view>`
})