import _ from '../libs/utils'

export default {
    /**
     * 获取首页公告列表
     * pi : page index, 从0开始，当小于0是表示全部获取
     * ps : page size, 当pi>=0时，ps>0
     * sort : 排序方式，默认为top，会按照置顶值来排序，可以不传，则按照添加顺序来排序。
     * 
     * 返回
     * ret : 公告列表
     * total: 总的公告数目
     */
    async listAnouces(pi, ps, sort) {
        return _.ajax.post({
            url: '/api/anouce/list',
            data: { pi: pi, ps: ps, sort:sort }
        });
    },

    /**
     * 获取公告详细
     * aid : 公告id
     * 
     * 返回
     * ret : 公告详细内容
     */
    async getAnouceDetail(aid) {
        return _.ajax.post({
            url: '/api/anouce/detail',
            data: { aid: aid }
        });
    },

    /**
     * 添加公告
     * token : 登录令牌
     * title : 标题
     * image : 上传的Image id
     * source : 来源
     * content : 内容
     * 返回
     * ret : 公告详细内容
     */
    async addAnouce(title, image, source,authorName,content) {
        let token = _.cookie.get("token");
        return _.ajax.post({
            url: '/api/anouce/add',
            data: { token: token, title: title, image: image, source: source,authorName:authorName,content: content }
        });
    },

    /**
     * 编辑公告
     * token : 登录令牌
     * aid: 公告id
     * title : 标题
     * image : 上传的Image id, 设置为0表示不修改
     * source : 来源
     * content : 内容
     * 返回
     * ret : 公告详细内容
     */
    async modifyAnouce(aid, title, image, source, authorName, content) {
        let token = _.cookie.get('token');
        return _.ajax.post({
            url: '/api/anouce/modify',
            data: { token: token, aid: aid, title: title, image: image, source: source, authorName:authorName,content: content }
        });
    },
    
    /**
     * 删除公告
     * token : 登录令牌
     * aid: 公告id
     * 返回
     * 
     */
    async deleteAnouce(aid) {
        let token = _.cookie.get('token');
        return _.ajax.post({
            url: '/api/anouce/delete',
            data: { token: token, aid: aid }
        });
    },

    /**
     * 对公告置顶
     * nid : 公告id
     * 
     * 返回
     */
    async topAnouce(aid, topIt) {
        let token = _.cookie.get('token');
        return _.ajax.post({
            url: '/api/anouce/top',
            data: { token: token, aid: aid, topIt:topIt}
        });
    }
}