module.exports = {
    invalidParams:'invalidParams',
    invalidAccountOrPassword:'invalidAccountOrPassword',
    tokenExpired:'tokenExpired',
    sessionNotFound:'sessionNotFound',
    accountNotFound:'accountNotFound',
    passwordNotFound:'passwordNotFound',
    userExists:'userExists'
}