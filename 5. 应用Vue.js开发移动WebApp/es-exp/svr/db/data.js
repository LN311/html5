const users = require('./tables/user');
const article = require('./tables/article');
const topic = require('./tables/topic');

module.exports = {
    users:users,
    topic:topic,
    article:article,
    primary:{
        users:2,
        article:100,
        topic:100
    }
}