/**
 * id:文章的ID
 * title:文章的标题；如果type为2时，话题会继承该文章所回答的问题的话题。
 * author:作者id,对应user表的id
 * topics:话题,第一个为主要话题；如果type为2时，话题会继承该文章所回答的问题的话题。
 * type:1表示提问，2表示回答，3表示文章;
 * from:如果type为2,from指定了该文章所回答的问题。
*/
module.exports = [
    //参考：https://www.zhihu.com/question/274327410/answer/386108948
    {id:1, title:"有哪些「不按套路出牌」的操作？", author:1, topics:[互联网,生活,心理学,社会,套路], type:1, from:0, approval:0,  content:""},
    {id:2, title:"", author:1, topics:[], type:2, from:1, approval:23,  content:"触动商家蛋糕，后期文章可能会删除，建议大家不要收藏，直接花费三分钟尝试一下。普通人网购时看中销量和评价，其实销量都是刷出来的，通过内部价促销便是最常用的方法！很多人都不知道网购的套路！今天就介绍一下怎样获取商品的内部价！分享给大家，希望对大家有所帮助。"},
    //参考:https://zhuanlan.zhihu.com/p/36447477
    {id:3, title:"零基础如何靠写作月入三五千",  author:2, topics:[], type:3, from:0, approval:22, content:"以下内容发布于金老湿公号，分享写作变现的一些方法。<br/><br/>这是金老湿第126次跟你对话，不知不觉，在这个公号里，已经陆陆续续写过了126篇文字，粉丝也从0到了1万多。<br/>写作以前只是我的兴趣爱好，后来帮助我顺利转行，再后来又帮我在每个月工作之余多了几千块的收入，我的朋友，但凡有点空闲时间不知道怎么打发的，我都劝他们练习写作。"}
]