

module.exports = (app, options)=>{
    app.use('/api/auth', require('./api/auth'));
    app.use('/api/article', require('./api/article'));
}