const path = require('path')
const moment = require('moment')
const webpack = require('webpack')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const ExtractTextPlugin = require('extract-text-webpack-plugin')
const vuxLoader = require('vux-loader')

const time = moment().format('YYYYMMDDHHmm')

module.exports = vuxLoader.merge({
  entry: {
    index: './src/index.js'
  },
  output: {
    path: path.resolve('./dist'),
    publicPath: '/',
    filename: `js/[name].js?v=${time}`
  },
  resolve: {
    extensions: ['.js', '.less', '.vue']
  },
  devtool: 'source-map',
  module: {
    rules: [{
      test: /\.vue$/,
      loader: 'vue-loader',
      // options: {
      //   loaders: {
      //       css: ExtractTextPlugin.extract({
      //           loader: 'css-loader',
      //           fallback: 'vue-style-loader'
      //       })
      //   }
      // }
    }, {
      test: /\.js$/,
      loader: 'babel-loader',
      include: [path.resolve('src')],
      options: {
        presets: ["es2015", "es2016", "es2017"],
        //no generater runtime
        //https://stackoverflow.com/questions/33527653/babel-6-regeneratorruntime-is-not-defined
        plugins: [
          ["transform-runtime", {
            "polyfill": false,
            "regenerator": true
          }]
        ]
      }
    }, 
    {
        test: /\.less$/,
        use: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: ['css-loader', 'less-loader']
        })
    }, {
      test: /\.styl$/,
      use: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: ['css-loader', 'stylus-loader']
        })
    },
    {
      test: /\.css$/,
      use: ExtractTextPlugin.extract({
        fallback: 'style-loader',
        use: {
          loader: 'css-loader',
          options: { minimize: true }
        }
      })
    }, {
      test: /\.(png|jpe?g|gif|svg)(\?.*)?$/,
      loader: 'url-loader',
      options: {
          limit: 10000,
          name: `img/[name].[ext]?v=${time}`,
          publicPath: '../'
      }
    }, {
      test: /\.(eot|ttf|woff|otf|woff2)$/,
      loader: 'file-loader',
      options: {
        name: `fonts/[name].[ext]?v=${time}`,
        publicPath: '../'
      }
    }]
  },
  plugins: [
    new ExtractTextPlugin(`css/[name].css?v=${time}`),
    new webpack.optimize.CommonsChunkPlugin({
      name: ['vendor']
    }),
    new HtmlWebpackPlugin({
      filename: 'index.html',
      template: './src/index.html',
      chunks: ['vendor', 'index']
    })
  ]
}, {
  plugins: ['vux-ui', 'progress-bar', 'duplicate-style']
})