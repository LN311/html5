const http = require('http')
const httpProxy = require('http-proxy')
const mime = require('mime')
const fs = require('fs')
const path = require('path')
const version = require('../package').version

class HttpContext
{
    constructor(server, req, res){
        this.server = server;
        this.req = req
        this.res = res
        this.status = 200

        var tmp = req.url.split('?')
        this.url = req.url;
        this.target = tmp[0]
        this.query = tmp[1]
        this.method = req.method;
        this.log = `${this.req.method} - ${this.req.url}`
    }

    sendText(status, text) {
        this.status = status;
        this.res.writeHead(this.status , {
            'Content-Type': 'text/plain',
            'Server': 'Front-Server/' + version
        })
        this.res.end(text)
    }

    sendJson(json){
        this.status = 200;
        this.res.writeHead(this.status, {
            'Content-Type': 'text/json',
            'Server': 'Front-Server/' + version
        })
        this.res.end(JSON.stringify(json));
    }

    sendFile (file) {
        var type = mime.lookup(file)
        this.status = 200;
        this.res.writeHead(this.status , {
            'Content-Type': type,
            'Server': 'Front-Server/' + version
        })
        fs.createReadStream(file).pipe(this.res)
    }

    sendProxy(proxy, to){
        if (this.query) {
            this.req.url = `${this.target}?${this.query}`
        }
        proxy.web(this.req, this.res, {
            changeOrigin: true,
            target: to
        })
    }
}

class HttpServer{

    constructor(options){
        this.options = options;
        this.proxy = null;
        this.server = null;
        this.routers = {};
    }

    use(routes){
        for(var k in routes){
            this.routers[k] = routes[k];
        }
    }

    start(){
        var cfg = this.options;
        cfg.base = cfg.base || path.join(__dirname, '../dist') + ' index.html'
        cfg.fallback = cfg.fallback ? path.join(__dirname, '../', cfg.fallback) : ''

        this.proxy = httpProxy.createProxyServer({});
        this.proxy.on("proxyRes", (p,req,res)=>this.onProxyService(p,req,res));
        this.proxy.on('error', (e,req,res)=>this.onProxyError(e,req,res));


        this.server = http.createServer((req,res)=>this.onService(req, res));
        this.server.on('error', (err)=>this.onError(err));

        this.server.listen(cfg.port);
        console.log(`Front-Server listening on 127.0.0.1:${cfg.port}`)
    }

    onProxyService(proxy, req, res){
        if (!proxy.headers['content-type']) {
            proxy.headers['content-type'] = 'text/plain; charset=utf-8'
        }
    }

    onProxyError(err, req, res){
        console.error(err)
        var ctx = new HttpContext(this.server, req, res);
        ctx.sendText(500, '500 - proxy error');
    }

    onError(err){
        console.error(err)
    }

    onService(req, res){
        let ctx = new HttpContext(this, req, res)

        this._makeRewrite(ctx, this.options);

        if (this._makeProxy(ctx, this.proxy, this.options)) 
            return

        if (this._makeRoot(ctx, this.options)) 
            return

        if (this._makeBase(ctx, this.options)) 
            return

        ctx.sendText(404, '404 - not found')
    }

    _makeRewrite(ctx, options)
    {
        var rewrite = options.rewrite || {};
        var path = ctx.target;
        var target = ctx.target
        for (let key in rewrite) {
            let pattern = new RegExp(key)
            if (pattern.test(path)) {
                ctx.target = path.replace(pattern, rewrite[key])
                break;
            }
        }
        
        if (ctx.target != path) {
            ctx.log += ` -> ${ctx.target}`
            if (ctx.query) {
                ctx.log += `?${ctx.query}`
            }
        }
    }

    _makeProxy(ctx, proxy, options){
        var _options = options.proxy || {}
        var req = ctx.req;
        var res = ctx.res;
        for (let key in _options) {
            let pattern = new RegExp(key)
            if (pattern.test(ctx.target)) {
                console.log(`${ctx.log} -> ${_options[key]}${ctx.target}`)
                ctx.sendProxy(proxy, _options[key]);
                return true
            }
        }
    }

    _makeRoot(ctx, options){
        var root = options.root || {};
        for (let key in root) {
            let pattern = new RegExp(key)
            if (pattern.test(ctx.target)) {
                let [targetPath, index] = root[key].split(' ')
                let file = path.join(targetPath, ctx.target)
                console.log(ctx.log)
                this._makeFile(ctx, file, index, options);
                return true;
            }
        }
    }

    _makeBase (ctx, options) {
        if (options.base) {
            var base = options.base;
            let [targetPath, index] = base.split(' ')
            let file = path.join(targetPath, ctx.target)
            console.log(ctx.log)
            this._makeFile(ctx, file, index, options);
            return true;
        }
    }

    _makeFile(ctx, file, index, options){
        var fallback = options.fallback;
        fs.exists(file, exists => {
            if (exists) {
                this._makeFileWithIndex(ctx, file, index);
            }
            else if(ctx.url === '/favicon.ico'){
                ctx.sendFile(path.join(__dirname, 'favicon.ico'))
            } else if (fallback && ctx.url == '/') {
                ctx.sendFile(fallback);
            } else{
                ctx.sendText(404, '404 - not found');
            }
        })
    }

    _makeFileWithIndex(ctx, file, index){
        fs.stat(file, (err, stat) => {
            if (err) {
                ctx.sendText(500, '500 - internal error')
                return;
            }

            if (stat.isDirectory()) {
                if (index) {
                    file = path.join(file, index)
                    fs.exists(file, exists => {
                        if (exists)
                            ctx.sendFile(file);               
                        else
                            ctx.sendText(403, '403 - forbidden')
                    })
                } else {
                    ctx.sendText(403, '403 - forbidden')
                }
            } else {
                ctx.sendFile(file);
            }
        })
    }
}

module.exports = {
    HttpServer,
    HttpContext,
    start(options){
        var s = new HttpServer(options);
        s.start();
        return s;
    }
};