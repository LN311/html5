import 'vux/src/styles/reset.less'
import Vue from 'vue'
import VueRouter from 'vue-router'
import { ToastPlugin, LoadingPlugin, ConfirmPlugin } from 'vux'
import App from './components/app.c'

import store from './stores/store'
import api from './services/apis'
import routes from './routes'
import './assets/css/style.styl'


Vue.use(VueRouter);
Vue.use(ToastPlugin)
Vue.use(LoadingPlugin)
Vue.use(ConfirmPlugin)

const router = new VueRouter({
  routes,
  mode: 'hash'
})

const safePages = ['/login', '/register', '/guide'];
router.beforeEach((to, from, next) => {
  let toIndex = to.path;
  if (safePages.indexOf(toIndex) >= 0) {
    next();
    return;
  }

  store.common.showTabbar = true;
  api.auth.check().then(d => {
    if (d.stat === 'ok') {
      store.common.isLogin = true
      next()
    } else {
      store.common.isLogin = false
      next('login');
    }
  })
})

new Vue({
  data: store,
  router,
  render: h => h(App)
}).$mount('#app');