import { RouteConfig } from 'vue-router'
import LoginComponent from './components/auth/login.c'

import HomeComponent from './components/home/home.c'
import HomeActComponent from './components/home/home.act.c'
import HomeHotComponent from './components/home/home.hot.c'
import HomeDiscComponent from './components/home/home.disc.c'

import IdeaComponent from './components/idea/idea.c'
import MarketComponent from './components/market/market.c'
import MessageComponent from './components/message/message.c'
import SettingsComponent from './components/settings/settings.c'

export default [{
  path: '/',
  redirect: '/login'
}, {
  path: '/login',
  component: LoginComponent
}, {
  path: '/home',
  component: HomeComponent,
  redirect:'/home/act',
  children:[
    {path:'act', component: HomeActComponent},
    {path:'hot', component: HomeHotComponent},
    {path:'disc', component: HomeDiscComponent}
  ]
}, {
  path: '/idea',
  component: IdeaComponent
}, {
  path: '/market',
  component: MarketComponent
}, {
  path: '/message',
  component: MessageComponent
}, {
  path: '/settings',
  component: SettingsComponent
}]