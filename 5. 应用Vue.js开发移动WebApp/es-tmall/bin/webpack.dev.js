const path = require('path')
const moment = require('moment')
const webpack = require('webpack')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const ExtractTextPlugin = require('extract-text-webpack-plugin')

const time = moment().format('YYYYMMDDHHmm')

module.exports = {
  entry: {
    index: './src/index.js'
  },
  output: {
    path: path.resolve('./dist'),
    publicPath: '/',
    filename: `js/[name].js?v=${time}`
  },
  resolve: {
    extensions: ['.js', '.vue'],
    alias: {
      vue: `vue/dist/vue.common.js`
    }
  },
  devtool: 'source-map',
  module: {
    rules: [{
      test: /\.vue$/,
      exclude: /node_modules/,
      loader: 'vue-loader',
      options: {
        extractCSS: true
      }
    }, {
      test: /\.js$/,
      exclude: /node_modules/,
      loader: 'babel-loader',
      options: {
        presets: ["es2015", "es2016", "es2017"],
        //no generater runtime
        //https://stackoverflow.com/questions/33527653/babel-6-regeneratorruntime-is-not-defined
        plugins: [
          ["transform-runtime", {
            "polyfill": false,
            "regenerator": true
          }]
        ]
      }
    }, {
      test: /\.css$/,
      use: ExtractTextPlugin.extract({
        fallback: 'style-loader',
        use: {
          loader: 'css-loader',
          options: { minimize: true }
        }
      })
    }, {
      test: /\.(png|jpg|jpeg|gif)$/,
      loader: 'file-loader',
      options: {
        name: `img/[name].[ext]?v=${time}`,
        publicPath: '../'
      }
    }, {
      test: /\.(svg|eot|ttf|woff|otf|woff2)$/,
      loader: 'file-loader',
      options: {
        name: `fonts/[name].[ext]?v=${time}`,
        publicPath: '../'
      }
    }]
  },
  plugins: [
    new ExtractTextPlugin(`css/[name].css?v=${time}`),
    new webpack.optimize.CommonsChunkPlugin({
      name: ['vendor']
    }),
    new HtmlWebpackPlugin({
      filename: 'index.html',
      template: './src/index.html',
      chunks: ['vendor', 'index']
    }),
    new webpack.ProvidePlugin({
      'window.Quill': 'quill/dist/quill.js',
      'Quill': 'quill/dist/quill.js',
    })
  ]
}