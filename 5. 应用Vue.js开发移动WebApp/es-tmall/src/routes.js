import { RouteConfig } from 'vue-router'

import FrameComponent from './components/common/frame.component'

import LoginComponent from './components/auth/login.component'

import AnouceListComponent from './components/anouce/anouce.list.component'
import AnouceAddComponent from './components/anouce/anouce.add.component'

export default [{
  path: '/',
  redirect: '/login'
}, {
  path: '/login',
  component: LoginComponent
}, {
  path: '/anouce',
  component: FrameComponent,
  redirect: '/anouce/list',
  children: [{
    path: 'list',
    component: AnouceListComponent
  },{
    path: 'add',
    component: AnouceAddComponent
  }]
}]