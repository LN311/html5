export default {
    match(stat){
        return this[stat] || '未知错误';
    },
    networkError:'网络错误',
    MethodNotFound:"服务端接口不存在"
}