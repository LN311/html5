import cookie from './cookie';
import ajax from './ajax';
import bytes from 'bytes'
import moment from 'moment'

export default {
    cookie,
    ajax,
    /**
     * 格式化显示日期时间
     * @param value 
     */
    dateTime(value) {
        return moment(value).format('YYYY-MM-DD HH:mm')
    },

    /**
     * 格式化显示字节大小
     * @param value 
     */
    bytes(value) {
        if (!value) {
            return '0 KB'
        }
        return bytes.format(value, {
            unitSeparator: ' '
        }).replace('kB', 'KB')
    }
}