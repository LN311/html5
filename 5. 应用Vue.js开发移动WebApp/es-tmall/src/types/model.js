/**
 * 用户信息
 */
export class UserInfo {
  constructor() {
    /**
   * 用户ID
   */
    this.uid = 0
    /**
     * 用户账号，用于显示
     */
    this.account = ''
    /**
     * 所属租户ID
     */
    this.cid = 0
    /**
     * 用户创建时间
     */
    this.ctime = 0
    /**
     * 用户邮箱
     */
    this.email = ''
    /**
     * 用户登录名
     */
    this.name = ''
    /**
     * 用户昵称
     */
    this.nickName = ''
    /**
     * 用户状态值
     */
    this.status = 0
  }

}