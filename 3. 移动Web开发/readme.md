移动Web基础
==========================
# 移动端页面设计规范(尺寸，自适应, 切图)
## [规范](http://www.ui.cn/detail/203306.html)
## 自适应（屏幕大小）
## 切图
Ios 避免锯齿 2倍尺寸

# viewport/meta
[参考](https://www.cnblogs.com/2050/p/3877280.html)

# 屏幕自适应(rem/vw的使用）
[单位](https://www.w3schools.com/cssref/css_units.asp)
[vh](http://www.zhangxinxu.com/wordpress/2012/09/new-viewport-relative-units-vw-vh-vm-vmin/)


# 移动Web特别样式处理
[参考](http://www.imooc.com/article/13501)
[参考](https://github.com/sunmaobin/sunmaobin.github.io/issues/28)
## 高清图片
## 1像素边框
## 相对单位rem
## 多行文本溢出

# Github博客
# 各种资源和样式
## [Less](https://less.bootcss.com/)
## [Sass](http://sass.bootcss.com/docs/sass-reference/)
## [Bootstrap](http://www.bootcss.com/)

# 作业
## 58同城移动版
## 相册（github)
## 猎豹动态效果
