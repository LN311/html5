课后作业
========================
# 作业内容
从以下三个作业中任选其一完成即可。

## 1. 58同城移动端界面（移动Web）
实现[房产][1]页面

* 整个页面实现
* 在chrome的手机模拟器中，最少适配iphone6/7/8；期望适配ipad；
* 最少支持竖屏，期望支持横屏
* 不一定要参考58的代码实现

## 2. 猎豹移动页面（html5+css3)
* 在第二节课作业的基础上，完成全部模块，并完成页面中动画部分。
* 适配iphone6/7/8,ipad（chrome模拟器中可查看）
* 期望支持横屏

## 3. github个人首页+相册
* 注册github账号。
* 实现自己的首页，可以自由设计，但包含以下模块：
> 1. 相册功能（Gallery或者Swiper实现均可，但要包含适当的动画效果）.
> 2. 日志列表（可以是虚拟的日志）
> 3. 关于自己（article标签实现）
> 4. 联系自己（链接，提交信息）
* 可参考但避免使用现成的模板
* 将设计好的首页上传到github
* 作业完成之后，在svn中提交ex03.md文档，内容是自己的github网页地址。

# 作业要求
* 三个作业任选其一完成即可。
* 在自己的svn目录下新建ex03目录，第三次作业提交到该目录。
* 作业在周五中午12点之前提交。

# 其他知识点
* [Github Pages][4]支持[Jekyll][3]作为静态网页生成器
* 非作业情况下，自己可以选择其它现成的[Jekyll模板][5]
* 一个比较容易理解的[Markdown教程][2]

[1]:http://m.58.com/wh/house.shtml
[2]:http://xianbai.me/learn-md/article/about/readme.html
[3]:http://jekyllcn.com/
[4]:https://pages.github.com/
[5]:http://jekyllthemes.org/