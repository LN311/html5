项目一、PC端页面布局基础
========================
# 1. HTML基础
(Hyper Text Markup Language)
## 基本页面
* DOCTYPE
* html,header,body
## 网页组成
* HTML:网页的内容，（是什么）
* CSS:网页的表现（长啥样)
* Javascript:网页的行为（能干啥）

## 基本标签
* [元素](http://www.runoob.com/html/html-elements.html)
* [属性](http://www.runoob.com/html/html-attributes.html)
* [区块](http://www.runoob.com/html/html-blocks.html)

## 练习
* a标签跳转
* img标签使用(注意大小)
* 标签嵌套

[看效果](http://www.runoob.com/html/html-quicklist.html)

# 2. CSS基础

### -CSS简介
* 层叠样式表（Cascading Style Sheets）
<!--
层叠样式表(英文全称：Cascading Style Sheets)是一种用来表现HTML（标准通用标记语言的一个应用）或XML（标准通用标记语言的一个子集）等文件样式的计算机语言。CSS不仅可以静态地修饰网页，还可以配合各种脚本语言动态地对网页各元素进行格式化。
CSS 能够对网页中元素位置的排版进行像素级精确控制，支持几乎所有的字体字号样式，拥有对网页对象和模型样式编辑的能力
-->
* 内容与表现分离（设计原则）
<!--
* 代码精简，文件小，打开快 （与table布局对比，可以流式显示）
* 皮肤
* 解耦，外部样式表可以极大提高工作效率
-->

### -CSS语法
<img src='assets/images/selector.gif'/>
<!--
选择器通常是我们需要改变样式的 HTML 元素。
每条声明由一个属性和一个值组成。
属性（property）是您希望设置的样式属性（style attribute）。每个属性有一个值。属性和值被冒号分开
-->

#### CSS的注释
``` css
/*段落的样式*/
p{
    /* 让文字居中显示 */
    text-align:center;
}
```

### -CSS选择器
#### 定义
需要改变样式的html元素
选择器的工作原理：选择器是从右我往左查找
#### 常见的选择器类型
[参考](http://www.runoob.com/cssref/css-selectors.html)
* [Id](http://www.runoob.com/css/css-id-class.html)
* [Class](http://www.runoob.com/css/css-id-class.html)
* [属性选择器](http://www.runoob.com/css/css-attribute-selectors.html)
* [伪类](http://www.runoob.com/css/css-pseudo-classes.html)
* [伪元素](http://www.runoob.com/css/css-pseudo-elements.html)

#### 组合选择符
[四种组合方式](http://www.runoob.com/css/css-combinators.html)

## 练习
[id class选择器]
[a标签伪类](http://www.runoob.com/css/css-link.html)


# 3. CSS三大特性
## 继承
当标签之间属于一种嵌套关系时，子元素可以继承父元素的样式
text-，font-，line-这些元素开头的属性、以及color等文字相关的属性都可以继承
特殊性：
1. a标签的颜色不能继承，必须对a标签本身进行设置
2. h标签的字体大小不能修改，必须对h标签本身进行修改
3. div 标签的高度如果不设置由内容来决定（没有内容高度为0），宽度默认由父元素继承过来
## 层叠
是浏览器处理冲突的一种机制：浏览器的渲染机制是从上到下，当出现冲突时会采用最后的那个样式，即样式的覆盖。
注意点：层叠性只有在多个选择器中选中“同一个标签”，然后又设置了“相同的属性”，才会发生层叠性
## 优先级
!important>行内样式>id选择器>类选择器>标签选择器>通配符>继承>浏览器默认

# 4. CSS盒子模型
## [参考](http://www.runoob.com/css/css-boxmodel.html)
## 居中

# 5. 容器，溢出及元素类型
* overflow:auto 内部元素大小自动撑开父元素
# 浮动、定位
* z-index需要absolute，而absolute会定位到


# 总结：
* html+js+css每一个都是一本书，不可能在短短几节课就能概括全面
* 方法 看文档，查资料
* 思想 怎么将UI设计转成程序设计