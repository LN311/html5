课后作业
========================
# 作业内容
以[天猫主页](http://www.tmall.com)为基础，完成该页面的site-nav,header,footer三个模块。

# 要求
1. 有基础的同学可以完成全部三个模块，没有基础的同学可以【任意选择其中2个模块】或者【仅做header模块】。
2. 学会使用Chrome工具查看对应模块的文档结构（html标签结构），大家可以照搬该文档结构（未显示的元素可不用写入）。
3. 学会使用Chrome工具分析元素css样式的构成，大家可以直接使用分析到的css样式。
4. 最迟周五提交作业到SVN，以ex01作为代码目录。

# 参考

## 基础代码 
见```/HTML5/1. PC端页面布局基础项目/demo```
里面会包含一些图片，字体资源，大家可以参考。

## 如何得到最简文档结构
* 打开开发者工具，在Elements中找到mallPage, 然后剔除其它无用的元素
![剔除无用元素](assets/images/ex-01.png)
* 分析mallPage构成，保留site-nav,header,footer元素，剔除其它元素。
![找到site-nav,header,footer](assets/images/ex-02.png)
* 关闭开发者工具，可以看到最简文档结构
![最简文档结构](assets/images/ex-03.png)
## 如何查看元素的css样式
1. 如果我们不知道为什么某个元素长成那个样子，我们就找到那个元素，去查看他的Styles。
![查看元素的CSS样式](assets/images/ex-04.png)
2. 如果我们想让他也张那个样子，就把Styles里面有用的样式内容拷贝到我们的css代码中。
